import Vue from 'vue';
import VueI18n from 'vue-i18n';
import MessagesEn from '../../../lang/en.json';
import MessagesFr from '../../../lang/fr.json';
import { getNavigatorLocale } from '../utils.js';

Vue.use(VueI18n);

let messages = {
  en: MessagesEn,
  fr: MessagesFr,
};
const numberFormats = {
  'en': {
    currency: {
      style: 'currency', currency: 'USD'
    }
  },
  'fr': {
    currency: {
      style: 'currency', currency: 'USD'
    }
  }
};

const locale = window.localStorage.getItem('lang') || getNavigatorLocale();

const i18n = new VueI18n({
  locale: locale || 'en', // set locale
  fallbackLocale: 'en',
  messages, // set locale messages
  numberFormats
});

export default i18n;
