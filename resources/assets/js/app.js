/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap.js';
import Vue from 'vue';
import * as _ from 'lodash';
import moment from 'moment';
import Quasar from 'quasar-framework';
import Vuex from 'vuex';
import VueConfig from 'vue-config';
import configs from './config.js';
import VueHead from 'vue-head';
import TranslateRoutePlugin from './router/TranslateRoutePlugin.js';
import * as VueGoogleMaps from 'vue2-google-maps';
import VueAnalytics from 'vue-analytics';
import router from './router/index.js';
import { capitalize } from './utils.js';
import * as constants from './constants.js';
import axios from 'axios';
import Auth from './services/auth.js';
import i18n from './i18n/index.js';
import store from './vuex/store.js';
import * as mutationTypes from './vuex/mutation-types.js';
import { Platform } from 'quasar-framework';
// eslint-disable-next-line import/extensions
import 'quasar-extras/roboto-font';
import Layout from './components/Layout.vue';


// Load the right Quasar theme based on which platform we're targetting.
if (Platform.is.cordova && Platform.is.ios) {
  // eslint-disable-next-line no-undef
  require('../../themes/ios.variables.styl');
} else {
  // eslint-disable-next-line no-undef
  require('../../themes/mat.variables.styl');
}

Vue.component('layout', Layout);

// //https://stackoverflow.com/questions/37694243/using-lodash-in-all-of-vue-component-template
Object.defineProperty(Vue.prototype, '$_', { value: _ });
Object.defineProperty(Vue.prototype, '$moment', { value: moment });
Object.defineProperty(Vue.prototype, '$configs', { value: configs });
Object.defineProperty(Vue.prototype, '$constants', { value: constants });

Vue.config.devtools = true;

// Vue Use
Vue.use(Quasar);
Vue.use(Vuex);

Vue.use(VueConfig, configs);
Vue.use(VueHead);
Vue.use(TranslateRoutePlugin);

Vue.use(VueGoogleMaps, {
  load: {
    key: configs.MIX_G_MAPS_KEY,
    libraries: 'places'
  }
});

// Filters
Vue.filter('capitalize', function (value) {
  return capitalize(value);
});

Vue.filter('tcap', function (str) {
  // https://stackoverflow.com/questions/42755664/capitalize-first-letter-of-each-word-in-js
  return str.toLowerCase().split(' ').map(x => x[0].toUpperCase() + x.slice(1)).join(' ');
});

Vue.filter('acap', function (str) {
  //https://www.npmjs.com/package/lodash.uppercase
  return _.upperCase(str);
});


// See https://stackoverflow.com/a/19581741
if (Platform.is.cordova) {
  // In Cordova, we need to properly define a server to make AJAX requests to.
  axios.defaults.baseURL = configs.MIX_APP_URL;
}
// Intercepts
axios.interceptors.request.use(function (config) {
  if (localStorage.token) {
    config.headers.Authorization = Auth.getJwtBearer();
  }
  if (config.data && !config.data.locale) {
    config.data.locale = i18n.locale;
  }

  return config;
}, function (err) {
  console.log('interceptors.request: ', err); // eslint-disable-line no-console
  // return Promise.reject(err);
});

axios.interceptors.response.use(response => {
  return response;
}, error => {
  let originalRequest = error.config;
  if ((error.response.status === 401) && !originalRequest._retry) {
    originalRequest._retry = true;
    store.dispatch('logout').then(() => {
      router.push({ name: 'home.' + i18n.locale });
    });
  }
  // if ((error.response.status === 403) && !originalRequest._retry) {
  //     originalRequest._retry = true;
  //     router.push({name: 'page-not-found'});
  // }

  return Promise.reject(error);
});

Quasar.start(() => {
  new Vue({
    el: '#app',

    computed: {
      ...Vuex.mapState(['user', 'authenticated', 'locale']),

    },
    created() {
      if (this.authenticated) {
        this.$i18n.locale = this.user.locale;
        this.$store.commit(mutationTypes.SET_LOCALE, this.user.locale);
      } else if (this.locale) {
        this.$i18n.locale = this.locale;
      }
    },

    i18n,
    store,
    router
  });
});
