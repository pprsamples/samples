import i18n from '../i18n/index.js';

export default {
  install: (Vue) => {
    Vue.prototype.$troute = function (routeName) {
      return `${routeName}.${i18n.locale}`;
    };
  }
};