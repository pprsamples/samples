@component('mail::message')
# New Account
Hello {{$content['name']}},
Congratulations! your account is now active!

@component('mail::button', ['url' => $url])
Visit Site
@endcomponent


Thanks,<br>
{{ __('docallo') }}
@endcomponent


