@component('mail::message')
# Nouveau compte
Salut {{$content['name']}},
Félicitation! Ton compte viens d'être créé!

@component('mail::button', ['url' => $url])
Aller au site
@endcomponent


Merci,<br>
{{ __('docallo')}}
@endcomponent


