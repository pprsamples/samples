<?php

use App\Entities\Auth\Role;
use App\User;
use Illuminate\Support\Facades\Event;

class UserTest extends TestCase
{
    protected $url = '/api/users/';

    /** @test */
    public function create_user_creates_patients()
    {
        $user = factory(App\User::class)->make();

        Event::fake();
        $response = $this->post($this->url, [
                'full_name' => $user->full_name,
                'email'     => $user->email,
                'password'  => $user->password,
            ]
        )->assertStatus(200);
        Event::assertDispatched(\App\Events\Users\Registered::class);

        $this->assertDatabaseHas('users', [
                'full_name' => $user->full_name,
                'email'     => $user->email,
            ]
        );

        // Creates Patients
        $decodedResponse = $response->decodeResponseJson();
        $userId = $decodedResponse['data']['id'];
        $role = Role::where('name', Role::PATIENT)->first();
        $this->assertDatabaseHas('role_user', [
                'user_id' => $userId,
                'role_id' => $role->id,
            ]
        );

        // Activation code generated
        $foundUser = User::where('id', $userId)
            ->whereNotNull('activation_code')
            ->first();
        $this->assertNotNull($foundUser);
    }

    /** @test */
    public function create_user()
    {
        /** @var \App\User $user */
        $user = factory(App\User::class)->make();

        Event::fake();
        $response = $this->post(
            $this->url, [
                'full_name' => $user->full_name,
                'email'     => $user->email,
                'password'  => $user->password,
            ]
        )->assertStatus(200)->json();
        Event::assertDispatched(\App\Events\Users\Registered::class);

        $role = Role::user()->first();
        $this->assertDatabaseHas('role_user', [
                'user_id' => $response['data']['id'],
                'role_id' => $role->id,
            ]
        );

        $this->assertDatabaseHas('users', [
                'full_name' => $user->full_name,
                'email'     => $user->email,
            ]
        );
    }

    /** @test */
    public function create_user_missing_email()
    {
        /** @var \App\User $user */
        $user = factory(App\User::class)->make();
        $this->json('POST',
            $this->url, [
                'full_name' => $user->full_name,
                'password'  => $user->password,
            ]
        )->assertStatus(422)->assertJsonStructure(['email']);
    }

    /** @test */
    public function create_user_missing_full_name()
    {
        $user = factory(App\User::class)->make();

        $this->json('POST',
            $this->url, [
                'password' => $user->password,
            ]
        )->assertStatus(422)->assertJsonStructure(['email']);
    }

    /** @test */
    public function create_user_duplicate_email()
    {
        $user = factory(App\User::class)->create();

        $this->json('POST',
            $this->url, [
                'full_name' => $user->full_name,
                'email'     => $user->email,
                'password'  => $user->password,
            ]
        )->assertStatus(422)->assertJsonStructure(['email']);
    }

    /** @test */
    public function user_index_no_token()
    {
        factory(App\User::class)->create();
        $this->json('GET', '/api/admin/users')
            ->assertStatus(400)
            ->assertJson(['error' => 'token_not_provided']);
    }

    /** @test */
    public function user_index_invalid_token_role()
    {
        $user = UserFactory::createPatient();
        $authHeader = UserFactory::getAuthorizationHeader($user);
        $this->json('GET', '/api/admin/users', [], $authHeader)->assertStatus(401);
    }

    /** @test */
    public function user_index_admin_role()
    {
        $user = UserFactory::createAdmin();
        $authHeader = UserFactory::getAuthorizationHeader($user);
        $this->json('GET', '/api/admin/users', [], $authHeader)->assertStatus(200);
    }

}
