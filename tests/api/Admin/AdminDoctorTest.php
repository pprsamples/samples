<?php namespace Tests\Api\Admin;

use App\Entities\Clinic;
use App\Entities\Doctor;
use App\Entities\Procedure;
use App\Entities\Specialty;
use TestCase;
use UserFactory;

class AdminDoctorTest extends TestCase
{
    /** @var string */
    protected $url = '/api/admin/doctors/';

    public function setUp()
    {
        parent::setUp();
        if (Doctor::typeExists() == true) {
            Doctor::deleteIndex();
        }
        Doctor::createIndexWithCustomAnalyzer();

        $user = UserFactory::createAdmin();
        $this->authHeader = UserFactory::getAuthorizationHeader($user);
    }

    public function tearDown()
    {
        Doctor::deleteIndex();
        parent::tearDown();
    }

    /** @test */
    public function index()
    {
        /** @var Clinic $clinic */
        $clinic = factory(Clinic::class)->create();
        /** @var Doctor $doctor */
        $doctors = factory(Doctor::class, 5)->create()
            ->each(function (Doctor $doctor) use ($clinic) {
                $specialty = factory(Specialty::class)->create();
                $doctor->specialties()->attach($specialty);
                \AppointmentFactory::specialty([
                    'doctor_id' => $doctor->id,
                    'meta'      => ['doctor_id' => $doctor->id],
                ]);
            });
        $response = $this->get($this->url, $this->authHeader)->assertStatus(200)->json();
        $jsonDoctors = $response['data'];
        $this->assertEquals($doctors->count(), count($jsonDoctors));
        $appointments = $jsonDoctors[0]['appointments']['data'];
        $this->assertEquals(1, count($appointments));
    }

    /** @test */
    public function update_doctor()
    {
        $this->disableExceptionHandling();
        /** @var Doctor $doctor */
        $doctor = factory(Doctor::class)->create(['status' => Doctor::STATUS_NON_REGISTERED]);
        $doctor->addToIndex();
        /** @var Specialty $specialty */
        $specialty = factory(Specialty::class)->states(['enabled'])->create();
        /** @var Procedure $specialty */
        $procedure = factory(Procedure::class)->states(['enabled'])->create();

        $newDoctor = factory(Doctor::class)->make();
        $firstName = $newDoctor->first_name;
        $lastName = $newDoctor->last_name;
        $email = $newDoctor->email;
        $this->json('PUT', $this->url . $doctor->id, [
            'first_name'    => $firstName,
            'last_name'     => $lastName,
            'email'         => $email,
            'specialty_ids' => [$specialty->id],
            'procedure_ids' => [$procedure->id],
        ], $this->authHeader)->assertStatus(200);

        $this->assertDatabaseHas('doctors', [
                'first_name' => $firstName,
                'last_name'  => $lastName,
                'email'      => $email,
            ]
        );

        $this->assertDatabaseHas('doctor_specialty', [
                'doctor_id'    => $doctor->id,
                'specialty_id' => $specialty->id,
            ]
        );

        $this->assertDatabaseHas('doctor_procedure', [
                'doctor_id'    => $doctor->id,
                'procedure_id' => $procedure->id,
            ]
        );
    }

    /** @test */
    public function store_doctor()
    {
        /** @var Doctor $doctor */
        $doctor = factory(Doctor::class)->make();
        $specialty = factory(Specialty::class)->create();
        $clinic = factory(Clinic::class)->create();
        $response = $this->json('POST', $this->url, [
            'first_name'    => $doctor->first_name,
            'last_name'     => $doctor->last_name,
            'email'         => $doctor->email,
            'gender'        => $doctor->gender,
            'clinic_id'     => $clinic->id,
            'specialty_ids' => [$specialty->id],
        ], $this->authHeader)->assertStatus(200)->json();

        $this->assertDatabaseHas('doctors', [
                'first_name' => $doctor->first_name,
                'last_name'  => $doctor->last_name,
                'email'      => $doctor->email,
                'gender'     => $doctor->gender,
            ]
        );

        $this->assertDatabaseHas('clinic_doctor', [
                'clinic_id' => $clinic->id,
                'doctor_id' => $response['data']['id'],
            ]
        );
        $this->assertDatabaseHas('doctor_specialty', [
                'specialty_id' => $specialty->id,
                'doctor_id'    => $response['data']['id'],
            ]
        );
    }

    /** @test
     * @throws \Elasticquent\Exception
     */
    public function update_doctor_specialties()
    {
        /** @var Doctor $doctor */
        $doctor = factory(Doctor::class)->create(['status' => Doctor::STATUS_NON_REGISTERED]);
        $doctor->addToIndex();

        $specialties = factory(Specialty::class, 3)->create();
        $specialtyIds = $specialties->pluck('id');

        $this->json('PUT', $this->url . $doctor->id, [
            'specialty_ids' => $specialtyIds,
        ], $this->authHeader)->assertStatus(200);

        foreach ($specialtyIds as $specialtyId) {
            $this->assertDatabaseHas('doctor_specialty', [
                    'doctor_id'    => $doctor->id,
                    'specialty_id' => $specialtyId,
                ]
            );
        }
    }
}
