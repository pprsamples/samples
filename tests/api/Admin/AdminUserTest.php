<?php namespace Tests\Api\Admin;

use App\Entities\Auth\Role;
use App\Entities\Clinic;
use App\User;
use TestCase;
use UserFactory;

class AdminUserTest extends TestCase
{
    protected $url = '/api/admin/users/';

    public function setUp()
    {
        parent::setUp();
        $this->user = UserFactory::createAdmin();
        $this->authHeader = UserFactory::getAuthorizationHeader($this->user);
    }

    /** @test */
    public function staff_admin_cannot_create_admin_user()
    {

        /** @var User $user */
        $user = UserFactory::createStaff();
        $authHeader = UserFactory::getAuthorizationHeader($user);
        $password = str_random(8);
        /** @var Role $role */
        $role = Role::where('name', Role::ADMIN)->first();

        $user = factory(User::class)->make();
        $this->json('POST', $this->url, [
            'first_name'            => $user->first_name,
            'last_name'             => $user->last_name,
            'email'                 => $user->email,
            'password'              => $password,
            'password_confirmation' => $password,
            'role_ids'              => [$role->id],
        ], $authHeader)->assertStatus(403);
    }

    /** @test */
    public function cannot_create_user_with_invalid_role()
    {
        /** @var User $user */
        $user = factory(User::class)->make();
        $password = str_random(8);

        $countRoles = Role::count();
        $this->json('POST', $this->url, [
            'first_name'            => $user->first_name,
            'last_name'             => $user->last_name,
            'email'                 => $user->email,
            'password'              => $password,
            'password_confirmation' => $password,
            'role_ids'              => [$countRoles + 10],

        ], $this->authHeader)->assertStatus(422)->assertJsonStructure(['role_ids.0']);
    }

    /** @test */
    public function create_user_attaches_roles_and_clinics()
    {
        /** @var User $user */
        $user = factory(User::class)->make();
        $password = str_random(8);

        /** @var Clinic $clinic */
        $clinic = factory(Clinic::class)->create();

        /** @var Role $staffAdminRole */
        $staffAdminRole = Role::where('name', Role::CLINIC_ADMIN)->first();
        /** @var Role $staffRole */
        $staffRole = Role::where('name', Role::STAFF)->first();
        $response = $this->json('POST', $this->url, [
            'full_name'  => $user->full_name,
            'email'      => $user->email,
            'password'   => $password,
            'role_ids'   => [$staffAdminRole->id, $staffRole->id],
            'clinic_ids' => [$clinic->id],
        ], $this->authHeader)->assertStatus(200)->json();

        $userId = $response['data']['id'];
        $this->assertDatabaseHas('role_user', [
            'role_id' => $staffAdminRole->id,
            'user_id' => $userId,
        ]);
        $this->assertDatabaseHas('role_user', [
            'role_id' => $staffRole->id,
            'user_id' => $userId,
        ]);

        $this->assertDatabaseHas('clinic_user', [
            'clinic_id' => $clinic->id,
            'user_id'   => $userId,
        ]);

        $this->assertDatabaseMissing('users', [
            'id'              => $userId,
            'activated'       => 0,
            'activation_code' => null,
        ]);
    }

    /** @test */
    public function delete_soft_deletes()
    {
        $user = UserFactory::createPatient();

        $this->assertDatabaseHas('users', [
            'id'         => $user->id,
            'deleted_at' => null,
        ]);

        $this->json('DELETE', $this->url . $user->id, [], $this->authHeader)
            ->assertStatus(200)
            ->json();

        $this->assertDatabaseMissing('users', [
            'id'         => $user->id,
            'deleted_at' => null,
        ]);
    }
}
