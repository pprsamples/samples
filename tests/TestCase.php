<?php

use App\Entities\AppointmentType;
use App\Entities\Auth\Role;
use App\Exceptions\Handler;
use App\User;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    use DatabaseTransactions;

    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /** @var array */
    protected $authHeader;

    /** @var  User */
    protected $user;

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        $this->clearCache();

        if (file_exists(dirname(__DIR__) . '/.env.test')) {
            (new \Dotenv\Dotenv(dirname(__DIR__), '.env.test'))->load();
        }

        return $app;
    }

    protected function disableExceptionHandling()
    {
        $this->app->instance(ExceptionHandler::class, new class extends Handler
        {
            public function __construct()
            {
            }

            public function report(Exception $e)
            {
            }

            public function render($request, Exception $e)
            {
                throw $e;
            }
        });
    }

    /**
     * Clears Laravel Cache.
     */
    protected function clearCache()
    {
        $commands = ['clear-compiled', 'cache:clear', 'view:clear', 'config:clear', 'route:clear'];
        foreach ($commands as $command) {
            \Illuminate\Support\Facades\Artisan::call($command);
        }
    }

    public function storeRolesProvider()
    {
        return [
            Role::ADMIN        => [Role::ADMIN],
            Role::CLINIC_ADMIN => [Role::CLINIC_ADMIN],
            Role::STAFF        => [Role::STAFF],
        ];
    }

    public function appointmentTypeProvider()
    {
        return [
            AppointmentType::SPECIALTY => [AppointmentType::SPECIALTY_ID],
            AppointmentType::WALKIN    => [AppointmentType::WALKIN_ID],
            AppointmentType::PROCEDURE => [AppointmentType::PROCEDURE_ID],
        ];
    }
}
