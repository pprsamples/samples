<?php

use App\Entities\Auth\Role;
use App\Http\Requests\UserCreateAdminRequest;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserCreateAdminRequestTest extends TestCase
{
    use DatabaseTransactions;

    /** @var  UserCreateAdminRequest */
    protected $request;

    /** @var array */
    protected $rules;

    public function setUp()
    {
        parent::setUp();
        $this->request = new UserCreateAdminRequest();
        $this->rules = $this->request->rules();
    }

    /** @test */
    public function role_ids_must_exist_for_user_create_doctor()
    {
        $roles = \Illuminate\Support\Facades\DB::table('roles')->get();

        $role = $roles[0];
        $countRoles = count($roles);
        $rule = $this->rules['role_ids'];

        $v = $this->app['validator']->make(['role_ids' => [$role->id]], ['role_ids' => $rule]);
        $this->assertTrue($v->passes());

        $v = $this->app['validator']->make(['role_ids' => ''], ['role_ids' => $rule]);
        $this->assertFalse($v->passes());

        $v = $this->app['validator']->make(['role_ids' => [$countRoles + 5]], ['role_ids.*' => $rule]);
        $this->assertFalse($v->passes());
    }

    /**
     * @test
     * @dataProvider authorizedRolesAdminStaff
     *
     * @param string $roleName
     * @param bool   $authorized
     */
    public function staff_admin_authorized_to_create_provided_roles($roleName, $authorized)
    {
        $user = factory(User::class)->create();
        $role = Role::where('name', Role::CLINIC_ADMIN)->first();
        $user->attachRole($role);
        $this->actingAs($user);

        $adminRole = Role::where('name', $roleName)->first();
        $this->request->replace([
            'role_ids' => [$adminRole->id],
        ]);

        $isAuthorized = $this->request->authorize();
        $this->assertEquals($isAuthorized, $authorized);
    }

    public function authorizedRolesAdminStaff()
    {
        return [
            Role::PATIENT      => [Role::PATIENT, true],
            Role::STAFF        => [Role::STAFF, true],
            Role::CLINIC_ADMIN => [Role::CLINIC_ADMIN, true],
            Role::ADMIN        => [Role::ADMIN, false],
        ];
    }

    /**
     * @test
     * @dataProvider unauthorizedRolesStaff
     *
     * @param $roleName
     */
    public function staff_unauthorized_to_create_provided_roles($roleName)
    {
        $user = factory(User::class)->create();
        $role = Role::where('name', Role::STAFF)->first();
        $user->attachRole($role);
        $this->actingAs($user);

        $adminRole = Role::where('name', $roleName)->first();
        $this->request->replace([
            'role_ids' => [$adminRole->id],
        ]);

        $authorized = $this->request->authorize();
        $this->assertFalse($authorized);
    }

    public function unauthorizedRolesStaff()
    {
        return [
            [Role::ADMIN],
            [Role::STAFF],
            [Role::CLINIC_ADMIN],
        ];
    }
}
