<?php

use App\Entities\Clinic;
use App\Http\Requests\DoctorCreateRequest;

class DoctorCreateRequestTest extends TestCase
{
    /** @var array */
    protected $rules;

    public function setUp()
    {
        parent::setUp();
        $this->rules = (new DoctorCreateRequest())->rules();
    }

    /** @test */
    public function clinic_id_must_exist_for_user_create_doctor()
    {
        $clinic = factory(Clinic::class)->create();

        $clinicRule = $this->rules['clinic_id'];

        $v = $this->app['validator']->make(['clinic_id' => $clinic->id], ['clinic_id' => $clinicRule]);
        $this->assertTrue($v->passes());

        $v = $this->app['validator']->make(['clinic_id' => $clinic->id + 5], ['clinic_id' => $clinicRule]);
        $this->assertFalse($v->passes());
    }

    public function accepts_includes()
    {
        factory(Clinic::class)->create();

        $this->asserttrue(array_key_exists('include', $this->rules));
        $this->asserttrue(array_key_exists('includes', $this->rules));
    }
}
