<?php

use App\Http\Requests\UserUpdateAdminRequest;
use App\User;

class UserUpdateAdminRequestTest extends TestCase
{
    /** @var  UserUpdateAdminRequest */
    protected $request;

    /** @var array */
    protected $rules;

    public function setUp()
    {
        parent::setUp();
        $this->request = new UserUpdateAdminRequest();
        $this->rules = $this->request->rules();
    }

    /** @test */
    public function user_id_must_exist()
    {
        $rule = $this->rules['user_id'];
        $user = factory(User::class)->create();

        $v = $this->app['validator']->make(['user_id' => [$user->id]], ['user_id' => $rule]);
        $this->assertTrue($v->passes());

        $v = $this->app['validator']->make(['user_id' => ''], ['user_id' => $rule]);
        $this->assertFalse($v->passes());

        $v = $this->app['validator']->make(['user_id' => [$user->id + 5]], ['user_id.*' => $rule]);
        $this->assertFalse($v->passes());
    }
}
