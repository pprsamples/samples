<?php namespace tests\unit\Entities;

use App\User;
use TestCase;

class UserTest extends TestCase
{
    /**
     * @test
     * @dataProvider firstNameProvider
     *
     * @param string $inputName
     * @param string $outputName
     */
    public function first_name_is_uppercase($inputName, $outputName)
    {
        /** @var User $user */
        $user = factory(User::class)->create(['first_name' => $inputName]);
        $this->assertEquals($user->first_name, $outputName);
    }

    public function firstNameProvider()
    {
        return [
            ['bob', 'Bob'],
            ['jean-pierre', 'Jean-Pierre'],
            ['christian raphael', 'Christian Raphael'],
        ];
    }
}
