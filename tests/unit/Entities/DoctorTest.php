<?php namespace tests\unit\Entities;

use App\Entities\Appointment;
use App\Entities\Doctor;
use App\Entities\ProcedureAppointmentMeta;
use App\Entities\SpecialtyAppointmentMeta;
use AppointmentFactory;
use Carbon\Carbon;
use DoctorFactory;
use TestCase;

class DoctorTest extends TestCase
{

    /** @test */
    public function appointments_method_returns_all_appointments_regardless_of_type()
    {
        /** @var Doctor $doctor */
        $doctor = DoctorFactory::withFees();
        AppointmentFactory::specialty([
            'doctor_id' => $doctor->id,
            'meta'      => [
                'doctor_id' => $doctor->id,
            ],
        ], 4);
        AppointmentFactory::procedure([
            'doctor_id' => $doctor->id,
            'meta'      => [
                'doctor_id' => $doctor->id,
            ],
        ]);

        $this->assertEquals(5, Appointment::count());
        $this->assertEquals(4, SpecialtyAppointmentMeta::count());
        $this->assertEquals(1, ProcedureAppointmentMeta::count());

        $appointments = $doctor->appointments;
        $this->assertEquals(5, $appointments->count());
    }

    /** @test */
    public function getIndexDocumentData_only_indexes_future_appointments()
    {
        $before = Carbon::now()->subDays(3);
        $after = Carbon::now()->addDays(3);

        /** @var Doctor $doctor */
        $doctor = DoctorFactory::withFees();
        AppointmentFactory::specialty([
            'meta'       => [
                'doctor_id' => $doctor->id,
            ],
            'doctor_id'  => $doctor->id,
            'status'     => Appointment::STATUS_AVAILABLE,
            'start_date' => $before->toDateString(),
        ], 2);
        AppointmentFactory::specialty([
            'meta'       => [
                'doctor_id' => $doctor->id,
            ],
            'doctor_id'  => $doctor->id,
            'status'     => Appointment::STATUS_AVAILABLE,
            'start_date' => $after->toDateString(),
        ], 1);

        $this->assertEquals(3, Appointment::count());

        $data = $doctor->getIndexDocumentData();
        $appointments = $data['appointments'];

        $this->assertEquals(1, count($appointments));
    }

}
