<?php namespace Tests\Unit\Mail;

use App\Mail\UserRegisteredEmail;
use App\User;
use Faker\Factory;
use TestCase;

class LocalizedMailableUnitTest extends TestCase
{
    /** @test */
    public function name_is_unchanged_if_view_does_not_exist()
    {
        $user = factory(User::class)->create();
        $userRegisteredEmail = new UserRegisteredEmail($user);

        $faker = Factory::create();
        $words = $faker->words($faker->numberBetween(1, 4));
        $viewName = implode('.', $words);

        $localizedViewName = $userRegisteredEmail->getLocalizedView($viewName, 'en');

        $this->assertEquals($viewName, $localizedViewName);
    }

    /** @test */
    public function name_is_suffixed_with_local()
    {
        $user = factory(User::class)->create();
        $userRegisteredEmail = new UserRegisteredEmail($user);

        $viewName = 'emails.patient.registered';

        $localizedViewName = $userRegisteredEmail->getLocalizedView($viewName, 'en');
        $this->assertEquals($viewName . '_en', $localizedViewName);

        $localizedViewName = $userRegisteredEmail->getLocalizedView($viewName, 'fr');
        $this->assertEquals($viewName . '_fr', $localizedViewName);
    }

    /** @test */
    public function name_is_suffixed_with_default_local_when_bad_locale_used()
    {
        $user = factory(User::class)->create();
        $userRegisteredEmail = new UserRegisteredEmail($user);

        $viewName = 'emails.patient.registered';

        $localizedViewName = $userRegisteredEmail->getLocalizedView($viewName, 'ru');
        $locale = config('app.locale');
        $this->assertEquals($viewName . '_' . $locale, $localizedViewName);
    }

}
