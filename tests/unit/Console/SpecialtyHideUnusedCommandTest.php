<?php

use App\Console\Commands\SpecialtyHideUnused;
use App\Entities\Appointment;
use App\Entities\Specialty;
use App\Entities\SpecialtyAppointment;
use App\Entities\SpecialtyAppointmentMeta;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SpecialtyHideUnusedCommandTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    function command_hides_specialties_with_no_available_appointments()
    {
        $specialties = factory(Specialty::class, 5)->create([
            'status' => Specialty::STATUS_ENABLED,
        ]);

        $appointments = collect();
        for ($i = 0; $i < 3; $i++) {
            $appointments->push(AppointmentFactory::specialty([
                'start_date'             => Carbon::now()->subMinutes(config('constants.INTERVAL_UNAVAILABLE'))->subMinute(),
                'status'                 => Appointment::STATUS_AVAILABLE,
                'meta' => [
                    'specialty_id' => $specialties[$i],
                ],
            ]));
        }
        $appointmentCount = SpecialtyAppointmentMeta::whereIn('appointment_id', $appointments->pluck('id'))
            ->count();
        $this->assertEquals(3, $appointmentCount);

        (new SpecialtyHideUnused((new SpecialtyAppointment)))->handle();

        $activeSpecialtyCount = Specialty::where('status', Specialty::STATUS_ENABLED)->count();
        $this->assertEquals(3, $activeSpecialtyCount);
        $hiddenSpecialtyCount = Specialty::where('status', Specialty::STATUS_HIDDEN)->count();
        $this->assertEquals(2, $hiddenSpecialtyCount);
    }

    /** @test */
    function command_does_not_affect_specialties_with_available_appointments()
    {
        $specialties = factory(Specialty::class, 3)->create([
            'status' => Specialty::STATUS_ENABLED,
        ]);

        $appointments = collect();
        $specialties->each(function(Specialty $specialty) use($appointments){
            $appointments->push(AppointmentFactory::specialty([
                'start_date'             => Carbon::now()->subMinutes(config('constants.INTERVAL_UNAVAILABLE'))->subMinute(),
                'status'                 => Appointment::STATUS_AVAILABLE,
                'meta' => [
                    'specialty_id' => $specialty->id,
                ],
            ]));
        });

        (new SpecialtyHideUnused((new SpecialtyAppointment)))->handle();
        $activeSpecialtyCount = Specialty::where('status', Specialty::STATUS_ENABLED)->count();

        $this->assertEquals(3, $activeSpecialtyCount);
    }

    /** @test */
    function command_activates_specialties_with_appointment()
    {
        $specialties = factory(Specialty::class, 3)->create([
            'status' => Specialty::STATUS_HIDDEN,
        ]);

        $appointments = collect();
        $specialties->each(function(Specialty $specialty) use($appointments){
            $appointments->push(AppointmentFactory::specialty([
                'start_date'             => Carbon::now()->subMinutes(config('constants.INTERVAL_UNAVAILABLE'))->subMinute(),
                'status'                 => Appointment::STATUS_AVAILABLE,
                'meta' => [
                    'specialty_id' => $specialty->id,
                ],
            ]));
        });

        (new SpecialtyHideUnused((new SpecialtyAppointment)))->handle();
        $activeSpecialtyCount = Specialty::where('status', Specialty::STATUS_ENABLED)->count();

        $this->assertEquals(3, $activeSpecialtyCount);
    }

}
