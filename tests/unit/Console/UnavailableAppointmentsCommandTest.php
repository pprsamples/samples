<?php

use App\Console\Commands\UnavailableAppointments;
use App\Entities\Appointment;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UnavailableAppointmentsCommandTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    function command_expires_available_appointments()
    {
        $appointment = AppointmentFactory::specialty([
            'start_date' => Carbon::now()->subMinutes(config('constants.INTERVAL_UNAVAILABLE'))->subMinute(),
            'status'     => Appointment::STATUS_AVAILABLE,
        ]);
        $this->assertDatabaseHas('appointments', [
            'status' => Appointment::STATUS_AVAILABLE,
        ]);

        (new UnavailableAppointments)->handle();

        $this->assertDatabaseHas('appointments', [
            'id'     => $appointment->id,
            'status' => Appointment::STATUS_UNAVAILABLE,
        ]);
    }

    /** @test */
    function command_cannot_expire_other_status_appointments()
    {
        $appointment = AppointmentFactory::specialty([
            'start_date' => Carbon::now()->subMinutes(config('constants.INTERVAL_UNAVAILABLE'))->subMinute(),
            'status'     => Appointment::STATUS_PENDING,
        ]);

        (new UnavailableAppointments)->handle();

        $this->assertDatabaseMissing('appointments', [
            'id'     => $appointment->id,
            'status' => Appointment::STATUS_UNAVAILABLE,
        ]);
    }

    /** @test */
    function command_does_not_modify_appointments_in_futur()
    {
        $appointment = AppointmentFactory::specialty([
            'start_date' => Carbon::now()->subMinutes(config('constants.INTERVAL_UNAVAILABLE'))->addMinute(),
            'status'     => Appointment::STATUS_AVAILABLE,
        ]);
        (new UnavailableAppointments)->handle();

        $this->assertDatabaseMissing('appointments', [
            'id'     => $appointment->id,
            'status' => Appointment::STATUS_UNAVAILABLE,
        ]);
    }
}
