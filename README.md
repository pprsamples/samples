# HelloDoc Application

### Official Laravel Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Notes

### Fractal

For more info on fractal and Transformers see [link](https://fractal.thephpleague.com/)

### Quasar Framework

These Vue.js samples were made using [v0.14 of the quasar framework](https://v0-14.quasar-framework.org/components/)

### Explanations

These are uncompleted samples of code taken from a Laravel v5.4 backend and a Vue 2 frontend project.

They contain most of the logic needed to sign up a new user to the website as well as the views that go with this.
- Verify inputs
- Create user in DB
- Send email to user

You will also find code that references doctors (or specialists) that have a modal, MySQL table, and ElasticSearch index associated to it.

Since many files have been omitted from the project, some paths may be non-existent while others refer to framework specific classes that were not included in the project.     

---

# Building Development Environment
The best development environment would be your own linux box OR a virtual machine. The best virtual machine and simplest would be from [Laravel Homestead](https://laravel.com/docs/5.2/homestead).

**NOTE**: For windows setup - please refer to the following page for some assistance since file system sync and path might differ from linux/mac [Laravel Homestead Windows installation](http://blog.teamtreehouse.com/laravel-homestead-on-windows)
## VM Installation
### Programs Needed
- [Vagrant] (https://www.vagrantup.com/downloads.html)
- [VirtualBox] (https://www.virtualbox.org/wiki/Downloads)
- [Git] (https://git-scm.com/downloads)
### Instructions
1. Install GIT,
2. Install VirtualBox,
3. Install Vagrant
4. Fork our project from: [Hellodoc](https://bitbucket.org/teamcybertron/hellodoc-core). To fork a project read this:[here](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)
5. Per Project Homestead Installation [link](https://laravel.com/docs/5.4/homestead#per-project-installation)
6. Create `.env` file from `.env.example`
7. `vagrant up` [Windows Instructions](https://laravelista.com/series/laravel-on-windows/part/2)
8. `vagrant ssh` into the VM
9. run `php artisan key:generate`

## ElasticSearch Installation

*Should be taken care of if you installed Homestead in project*

For search to yield results, the application needs elasticsearch ecosystem to be installed.

`sudo apt-get update`

`sudo apt-get install openjdk-8-jre -y`

`wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.0.1.deb`

`sudo dpkg -i elasticsearch-6.0.1.deb`

`sudo service elasticsearch start`

To verify status

`sudo service elasticsearch status`

Warning! This will take a while (3-5 seconds) to start in the background after the OK message because Java. 🙂
test if ElasticSearch HTTP service works (try again after some time if you get the ‘curl: (7) couldn’t connect to host’ message):

`curl -X GET 'http://localhost:9200'`

you should receive a JSON like this:
```
{
 "name" : "Maur-Konn",
 "cluster_name" : "elasticsearch",
 "version" : {
 "number" : "2.2.0",
 "build_hash" : "8ff36d139e16f8720f2947ef62c8167a888992fe",
 "build_timestamp" : "2016-01-27T13:32:39Z",
 "build_snapshot" : false,
 "lucene_version" : "5.4.1"
 },
 "tagline" : "You Know, for Search"
 }
```

#### [OPTIONAL] Setup Cerebro and make it available on local machine

`https://github.com/lmenezes/cerebro`

## ElasticSearch Uninstallation

```
sudo apt-get --purge autoremove elasticsearch
sudo rm -rf /var/lib/elasticsearch/
sudo rm -rf /etc/elasticsearch
```