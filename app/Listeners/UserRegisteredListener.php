<?php namespace App\Listeners;

use App\Events\Users\Registered;
use App\Jobs\EmailConfirmationJob;
use App\Notifications\AdminAlertNotification;
use App\Recipients\AdminRecipient;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class UserRegisteredListener
{
    use DispatchesJobs;

    /**
     * Handle the event.
     *
     * @param  Registered $event
     *
     * @return void
     */
    public function handle(Registered $event)
    {
        $user = $event->user;
        try {
            $this->dispatch(new EmailConfirmationJob($user));

            if (App::environment('production')) {
                $recipient = new AdminRecipient();
                $recipient->notify(new AdminAlertNotification('New Patient Registered - ' . config('app.url'),
                    '#patients',
                    [
                        'id'    => $user->id,
                        'name'  => $user->name,
                        'email' => $user->email,
                    ]
                ));
            }
        } catch (\Exception $e) {
            Log::critical($e);
        }
    }
}
