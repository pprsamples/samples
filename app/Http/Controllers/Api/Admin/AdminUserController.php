<?php namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\UserCreateAdminRequest;
use App\Http\Requests\UserGetRequest;
use App\Http\Requests\UserIndexRequest;
use App\Http\Requests\UserUpdateAdminRequest;
use App\Paginators\ElasticquentPaginatorAdapter;
use App\Transformers\UserTransformer;
use App\User;
use Elasticquent\ElasticquentPaginator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class AdminUserController extends ApiController
{

    /**
     * @var User
     */
    protected $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index(UserIndexRequest $request)
    {
        list($perPage, $offset, $limit) = $this->getPagination($request);

        $includes = $this->getIncludes($request);
        $usersQuery = $this->user->with($includes->toArray())
            ->orderBy('full_name', 'ASC');

        $users = $usersQuery->skip($offset)->take($perPage)->get();
        $paginator = $usersQuery->paginate($perPage);

        return fractal()->collection($users, new UserTransformer())
            ->parseIncludes($includes->toArray())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator));
    }

    public function search(UserIndexRequest $request)
    {
        list($perPage, $offset, $limit) = $this->getPagination($request);

        $includes = $this->getIncludes($request);
        $query = $request->get('query', '');
        $searchQuery = [
            'bool' => [
                'should' => [
                    'match' => [
                        'full_name' => [
                            'query'            => $query,
                            'zero_terms_query' => 'all',
                        ],
                    ],
                ],
            ],
        ];
        $sortQuery = [
            'full_name' => [
                'order'   => 'asc',
                'missing' => '_last',
            ],
        ];

        $source = Schema::getColumnListing($this->user->getTable());
        $users = $this->user->searchByQuery($searchQuery,
            null,
            $source,
            $limit,
            $offset,
            $sortQuery);

        return fractal()->collection($users, new UserTransformer())
            ->parseIncludes($includes->toArray())
            ->paginateWith(new ElasticquentPaginatorAdapter($users->paginate($perPage)));
    }

    /**
     * @param UserGetRequest $request
     * @param int            $id
     *
     * @return \Illuminate\Http\JsonResponse|\Spatie\Fractal\Fractal
     */
    public function get(UserGetRequest $request, $id)
    {
        try {
            /** @var User $user */
            $user = $this->user->findOrFail($id);

            $includes = $this->getIncludes($request, ['roles.perms']);
            $user->load($includes->toArray());

            return fractal()->item($user, new UserTransformer())->parseIncludes($includes->toArray());
        } catch (\Exception $e) {
            return $this->notFound('User not found');
        }
    }

    public function store(UserCreateAdminRequest $request)
    {
        try {
            $user = $this->user->create([
                'full_name'       => $request['full_name'],
                'first_name'      => $request['first_name'],
                'last_name'       => $request['last_name'],
                'email'           => $request['email'],
                'password'        => $request['password'],
                'locale'          => $request->input('locale', 'fr'),
                'activation_code' => str_random(30),
            ]);

            $user->roles()->sync($request->input('role_ids'));

            if ($request->has('clinic_ids')) {
                $user->clinics()->syncWithoutDetaching($request->input('clinic_ids'));
            }

            $user->save();

            return fractal()->item($user, new UserTransformer());
        } catch (\Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    public function update(UserUpdateAdminRequest $request, $id)
    {
        try {
            /** @var User $user */
            $user = $this->user->find($id);

            $user->username = $request->input('username', $user->username);
            $user->full_name = $request->input('full_name', $user->full_name);
            $user->first_name = $request->input('first_name', $user->first_name);
            $user->last_name = $request->input('last_name', $user->last_name);
            $user->zipcode = $request->input('zipcode', $user->zipcode);
            $user->gender = $request->input('gender', $user->gender);
            $user->email = $request->input('email', $user->email);
            $user->skype_id = $request->input('skype_id', $user->skype_id);
            $user->phone = $request->input('phone', $user->phone);
            $user->birthdate = $request->input('birthdate', $user->birthdate);

            if ($request->has('locale')) {
                $locale = $request->input('locale', $user->locale);
                if (in_array($locale, ['en', 'fr'])) {
                    $user->locale = $locale;
                }
            }

            if ($request->has('password')) {
                $user->password = $request->input('password');
            }
            if ($request->has('role_ids')) {
                $user->roles()->sync($request->input('role_ids'));
            }

            if ($request->has('clinic_ids')) {
                $user->clinics()->syncWithoutDetaching($request->input('clinic_ids'));
            }

            $user->save();

            $includes = $this->getIncludes($request, ['roles.perms']);
            $user->load($includes->toArray());

            return fractal()->item($user, new UserTransformer())->parseIncludes($includes->toArray());
        } catch (\Exception $e) {
            return $this->invalidData($e->getMessage());
        }
    }

    public function delete($id)
    {
        $deleted = $this->user->destroy($id);

        return response()->json(['deleted' => $deleted]);
    }
}
