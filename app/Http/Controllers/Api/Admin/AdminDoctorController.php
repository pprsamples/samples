<?php namespace App\Http\Controllers\Api\Admin;

use App\Entities\Clinic;
use App\Entities\Doctor;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\DoctorCreateAdminRequest;
use App\Http\Requests\DoctorGetAdminRequest;
use App\Http\Requests\DoctorImageRequest;
use App\Http\Requests\DoctorUpdateRequest;
use App\Http\Responses\JsonEmptyResponse;
use App\Transformers\DoctorTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\MediaLibrary\Media;

class AdminDoctorController extends ApiController
{
    /**
     * @var Doctor
     */
    protected $doctor;

    /**
     * @param Doctor $doctor
     */
    public function __construct(Doctor $doctor)
    {
        $this->doctor = $doctor;
    }

    /**
     * @param DoctorGetAdminRequest $request
     *
     * @return JsonEmptyResponse|\Spatie\Fractal\Fractal
     */
    public function index(DoctorGetAdminRequest $request)
    {
        list($perPage, $offset, $limit) = $this->getPagination($request);

        $includes = array_merge($this->doctor->getRelations(), ['clinics', 'appointments', 'fee_metas.fee']);

        $doctorsQuery = $this->doctor->with($includes);

        if ($request->has('specialty_id')) {
            $doctorsQuery->whereHas('specialty', function ($query) use ($request) {
                $query->where('specialty_id', (int)$request->get('specialty_id'));
            });
        }

        $statuses = $this->getStatuses($request);
        if (!empty($statuses)) {
            $doctorsQuery->whereIn('status', $statuses);
        }

        $doctors = $doctorsQuery->skip($offset)->take($perPage)->get();
        $doctorPaginator = $doctorsQuery->paginate($perPage);

        return fractal()->collection($doctors, new DoctorTransformer())
            ->parseIncludes($includes)
            ->paginateWith(new IlluminatePaginatorAdapter($doctorPaginator));
    }

    /**
     * @param DoctorCreateAdminRequest $request
     *
     * @return mixed
     */
    public function store(DoctorCreateAdminRequest $request)
    {
        try {
            $data = $request->only(['first_name', 'last_name', 'email', 'gender']);
            $data = array_merge($data, [
                'status' => Doctor::STATUS_NON_REGISTERED,
            ]);
            /** @var Doctor $doctor */
            $doctor = $this->doctor->create($data);
            $doctor->clinics()->attach($request->input('clinic_id'));

            $specialtyIds = $request->input('specialty_ids');
            $doctor->specialties()->sync($specialtyIds);

            $doctor->is_doctor = $doctor->isDoctor();

            $doctor->save();

            $includes = $this->doctor->getRelations();
            $doctor->load($includes)->addToIndex();

            return fractal()->item($doctor, new DoctorTransformer())->parseIncludes($includes);
        } catch (\PDOException $e) {
            return response()->json($e->getMessage());
        } catch (\Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    /**
     * @todo Return correct duplicate issue validation
     *
     * @param DoctorUpdateRequest $request
     * @param int                 $id
     *
     * @return \Spatie\Fractal\Fractal
     */
    public function update(DoctorUpdateRequest $request, $id)
    {
        try {
            /** @var Doctor $doctor */
            $doctor = $this->doctor->findOrFail($id);
            $doctor->first_name = $request->input('first_name', $doctor->first_name);
            $doctor->last_name = $request->input('last_name', $doctor->last_name);
            $doctor->gender = $request->input('gender', $doctor->gender);
            $doctor->status = $request->input('status', $doctor->status);
            $doctor->email = $request->input('email', $doctor->email);
            $doctor->bio_en = $request->input('bio_en', $doctor->bio_en);
            $doctor->bio_fr = $request->input('bio_fr', $doctor->bio_fr);

            if ($request->has('specialty_ids')) {
                $doctor->specialties()->sync($request->input('specialty_ids'));
            }
            if ($request->has('procedure_ids')) {
                $doctor->procedures()->sync($request->input('procedure_ids'));
            }

            $doctor->is_doctor = $doctor->isDoctor();

            $doctor->save();

            $includes = $this->doctor->getRelations();
            $doctor->load($includes);
            $doctor->updateIndex();

            return fractal()->item($doctor, new DoctorTransformer())->parseIncludes($includes);
        } catch (\PDOException $e) {
            return response()->json($e->getMessage(), 404);
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $deleted = Doctor::destroy($id);

        return response()->json(['deleted' => $deleted]);
    }

    /**
     * @param DoctorImageRequest $request
     * @param int                $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadImage(DoctorImageRequest $request, $id)
    {
        /** @var Doctor $doctor */
        $doctor = Doctor::findOrFail($id);
        if ($doctor->hasMedia('doctors.avatar')) {
            $doctor->clearMediaCollection('doctors.avatar');
        }
        $fileName = str_slug($doctor->name);

        $doctor->addMediaFromRequest('image')
            ->usingName($fileName)
            ->toMediaCollection('doctors.avatar');

        return response()->json([
            'avatar_path' => $doctor->fresh()->avatar_url,
        ]);
    }
}
