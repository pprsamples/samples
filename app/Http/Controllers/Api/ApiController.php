<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\IncludeRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ApiController extends Controller
{
    protected static $internalCode = [
        400 => 400,
        404 => 404,
        403 => 403,
        409 => 409,
        503 => 503,
        500 => 500,
    ];

    /**
     * @param $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function notFound($message)
    {
        return $this->error($message, 404);
    }

    /**
     * @param $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function invalidData($message)
    {
        return $this->error($message, 400);
    }

    /**
     * @param $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function unauthorized($message)
    {
        return $this->error($message, 403);
    }

    /**
     * @param $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function serverIssuesTryAgain($message)
    {
        return $this->error($message, 503);
    }

    /**
     * @param $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function internalIssues($message)
    {
        return $this->error($message, 500);
    }

    /**
     * @param $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function duplicate($message)
    {
        return $this->error($message, 409);
    }

    /**
     * @param $message
     * @param $code
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function error($message, $code)
    {
        return response()->json([
            'error' => [
                'message' => $message,
                'code'    => self::$internalCode[$code],
            ],
        ], $code);
    }

    public function json($data)
    {
        return response()->json(
            [
                'status_code' => '0',
                'content'     => $data,
            ],
            200
        );
    }

    public function ok()
    {
        return response(null, 200);
    }

    /**
     * Return pagination array [$perPage, $offset, $limit, $page];
     *
     * @param Request $request
     *
     * @return array
     */
    protected function getPagination(Request $request)
    {
        $perPage = $request->input('per_page', 100);
        $limit = $request->input('limit', 100);
        $page = $request->input('page', 1);
        $offset = ($page * $perPage) - $perPage;

        return [$perPage, $offset, $limit, $page];
    }

    /**
     * Return coordinates array ['lat', 'lon']
     *
     * @param Request $request
     *
     * @return array
     */
    protected function getCoordinates(Request $request)
    {
        $coordinates = [];
        if ($request->has('latitude') && $request->has('longitude')) {
            $params = $request->all();
            $coordinates = ['lat' => $params['latitude'], 'lon' => $params['longitude']];
        } elseif ($request->has('lat') && $request->has('lng')) {
            $coordinates = ['lat' => $request->get('lat'), 'lon' => $request->get('lng')];
        } elseif ($request->has('postal')) {
            $postal = urldecode($request->get('postal'));
            $coordinates = getCoordinates($postal);
        }

        return $coordinates;
    }

    /**
     * Get include|includes query param
     *
     * @param IncludeRequest $request
     * @param array          $defaults
     *
     * @return Collection
     */
    protected function getIncludes(IncludeRequest $request, array $defaults = [])
    {
        $includes = [];
        if ($request->has('include')) {
            $includes = [$request->input('include')];
        } elseif ($request->has('includes')) {
            $includes = $request->input('includes');
        }

        return collect(array_unique(array_merge($defaults, $includes)));
    }

    /**
     * @param IncludeRequest $request
     * @param array          $defaults
     *
     * @return array
     */
    protected function getStatuses(IncludeRequest $request, array $defaults = [])
    {
        if ($request->has('status')) {
            return [$request->input('status')];
        } elseif ($request->has('statuses')) {
            return $request->input('statuses');
        }

        return $defaults;
    }

    /**
     * @param IncludeRequest $request
     * @param array          $defaults
     *
     * @return array
     */
    protected function getRoles(IncludeRequest $request, array $defaults = [])
    {
        if ($request->has('role_id')) {
            return [$request->input('role_id')];
        } elseif ($request->has('role_ids')) {
            return $request->input('role_ids');
        } elseif ($request->has('roles')) {
            return $request->input('roles');
        }

        return $defaults;
    }
}
