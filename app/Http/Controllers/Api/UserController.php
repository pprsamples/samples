<?php namespace App\Http\Controllers\Api;

use App\Entities\Auth\Role;
use App\Events\Users\Registered;
use App\Http\Requests\CreateAdminUserRequest;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserImageRequest;
use App\Transformers\UserTransformer;
use App\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class UserController extends ApiController
{
    public function __construct()
    {
        $this->middleware(['api', 'jwt.auth'], ['except' => ['store', 'activate']]);
    }

    /**
     * @param UserCreateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|\Spatie\Fractal\Fractal
     */
    public function store(UserCreateRequest $request)
    {
        try {
            //Create User
            $user = User::create([
                'full_name'       => $request['full_name'],
                'email'           => $request['email'],
                'password'        => $request['password'],
                'locale'          => $request->input('locale', 'fr'),
                'activation_code' => str_random(30),
            ]);

            // Attach Patient role
            $userRole = Role::where('name', Role::PATIENT)->first();
            $user->attachRole($userRole);

            $user->save();

            event(new Registered($user));

            return fractal()->item($user, new UserTransformer());
        } catch (\PDOException $e) {
            return response()->json($e->getMessage());
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse|\Spatie\Fractal\Fractal
     */
    public function get($id)
    {
        try {
            /** @var User $user */
            $user = User::findOrFail($id);

            $includes = ['roles.perms'];

            return fractal()->item($user, new UserTransformer())->parseIncludes($includes);
        } catch (\PDOException $e) {
            return $this->notFound('User not found');
        }
    }

    /**
     * @param CreateAdminUserRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createAdmin(CreateAdminUserRequest $request)
    {
        DB::connection()->beginTransaction();
        $response = '';
        try {
            $user = User::create($request->all());
            $role = Role::where('name', '=', $request->input('role'))->first();
            //$user->attachRole($request->input('role'));
            $user->roles()->attach($role->id);
            $user->load('roles');
            DB::connection()->commit();

            return response()->json($user);
        } catch (\PDOException $e) {
            $response = response()->json($e->getMessage());
        } catch (\Exception $e) {
            $response = response()->json('An error occured');
        }
        DB::connection()->rollBack();

        return $response;
    }

    /**
     * @param UserImageRequest $request
     * @param int              $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    public function uploadImage(UserImageRequest $request, $id)
    {
        /** @var User $user */
        $user = User::findOrFail($id);
        if ($user->hasMedia('users.avatar')) {
            $user->clearMediaCollection('users.avatar');
        }
        $fileName = str_slug($user->name);

        $user->addMediaFromRequest('image')
            ->usingName($fileName)
            ->toMediaCollection('users.avatar');

        return response()->json([
            'avatar_path' => $user->fresh()->avatar_url,
        ]);
    }

    public function activate($code)
    {
        /** @var User $user */
        $user = User::where('activation_code', $code)->first();

        if (is_null($user)) {
            //@TODO no user to activate page
            return $this->invalidData(__('no user with code', ['code' => $code]));
        }

        $user->activated = 1;
        $user->activation_code = null;
        $user->save();

        return $this->ok();
    }
}
