<?php namespace App\Http\Controllers\Api\My;

use App\Entities\Doctor;
use App\Entities\Marker;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\AppointmentGetRequest;
use App\Http\Requests\UserGetRequest;
use App\Http\Requests\UserUpdateMarkerRequest;
use App\Http\Requests\UserUpdatePasswordRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Mail\VerifyEmail;
use App\Transformers\MarkerTransformer;
use App\Transformers\UserTransformer;
use App\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class MyProfileController extends ApiController
{
    public function get(UserGetRequest $request)
    {
        $includes = $this->getIncludes($request);
        /** @var $user User */
        $user = Auth::user();
        $user->load($includes->toArray());

        return fractal()->item($user, new UserTransformer())->parseIncludes($includes->toArray());
    }

    public function update(UserUpdateRequest $request)
    {
        try {
            /** @var User $user */
            $user = Auth::user();

            /** @var Doctor $this ->user */
            $user->username = $request->input('username', $user->username);
            $user->full_name = $request->input('full_name', $user->full_name);
            $user->first_name = $request->input('first_name', $user->first_name);
            $user->last_name = $request->input('last_name', $user->last_name);
            $user->zipcode = $request->input('zipcode', $user->zipcode);
            $user->gender = $request->input('gender', $user->gender);
            $user->email = $request->input('email', $user->email);
            $user->skype_id = $request->input('skype_id', $user->skype_id);
            $user->phone = $request->input('phone', $user->phone);
            $user->birthdate = $request->input('birthdate', $user->birthdate);

            if ($request->has('locale')) {
                $locale = $request->input('locale', $user->locale);
                if (in_array($locale, ['en', 'fr'])) {
                    $user->locale = $locale;
                }
            }
            $user->save();

            $includes = $this->getIncludes($request, ['roles.perms'])->toArray();
            $user->load($includes);

            return fractal()->item($user, new UserTransformer())->parseIncludes($includes);
        } catch (\Exception $e) {
            return $this->invalidData($e->getMessage());
        }
    }

    public function updatePassword(UserUpdatePasswordRequest $request)
    {
        try {
            /** @var User $user */
            $user = Auth::user();

            if ($request->has('password')) {
                $user->password = $request->input('password');
            }
            $user->save();

            $includes = $this->getIncludes($request, ['roles.perms']);
            $user->load($includes->toArray());

            return fractal()->item($user, new UserTransformer())->parseIncludes($includes->toArray());
        } catch (\Exception $e) {
            return $this->invalidData($e->getMessage());
        }
    }

    public function updateMarker(UserUpdateMarkerRequest $request)
    {
        try {
            $address = $request->input('address');
            $lat = $request->input('lat');
            $lng = $request->input('lng');
            /** @var User $user */
            $user = Auth::user();

            /** @var Marker $marker */
            $marker = Marker::firstOrCreate([
                'address' => $address,
                'lat'     => floatval($lat),
                'lng'     => floatval($lng),
            ]);

            $user->markers()->sync([$marker->id]);
            $user->save();

            return fractal()->item($marker, new MarkerTransformer());
        } catch (\Exception $e) {
            return $this->invalidData($e->getMessage());
        }
    }

    public function resendVerifyEmail()
    {
        /** @var User $user */
        $user = Auth::user();

        Mail::to($user->email)->send(new VerifyEmail($user));

        return response()->json([
            'message' => 'Your confirmation email has been resent',
        ], Response::HTTP_OK);
    }
}
