<?php namespace App\Http\Controllers\Api;

use App\Entities\Appointment;
use App\Entities\Doctor;
use App\Http\Requests\DoctorGetRequest;
use App\Http\Responses\JsonEmptyItemResponse;
use App\Transformers\DoctorTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use App\Paginators\ElasticquentPaginatorAdapter;

class DoctorController extends ApiController
{
    /** @var Doctor */
    protected $doctor;

    public function __construct(Doctor $doctor)
    {
        $this->doctor = $doctor;
    }

    public function index(Request $request)
    {
        list($perPage, $offset, $limit) = $this->getPagination($request);

        $ESQuery['function_score']['query']['bool']['should'] = [];
        $ESQuery['function_score']['query']['bool']['must']['term']['status'] = Doctor::STATUS_REGISTERED;

        if ($request->has('specialty_id')) {
            $ESQuery['function_score']['query']['bool']['filter'][]['term']['specialties.id'] = $request->get('specialty_id');
        }

        $query = $request->get('query', null);
        if (!is_null($query) && strlen($query) > 2) {
            $ESQuery['function_score']['query']['bool']['should'][] = [
                'multi_match' => [
                    'query'     => $query,
                    'fields'    => [
                        'name^8',
                        'city^2',
                        'address_*',
                    ],
                    'fuzziness' => 1,
                    'type'      => 'most_fields',
                    'operator'  => 'or',
                ],
            ];
            $ESQuery['function_score']['query']['bool']['should'][] = [
                'multi_match' => [
                    'query'    => $query,
                    'fields'   => [
                        'name^8',
                        'city^2',
                        'address_*',
                    ],
                    'type'     => 'cross_fields',
                    'operator' => 'or',
                ],
            ];
        }

        $coordinates = $this->getCoordinates($request);
        if (!empty($coordinates)) {
            $ESQuery['function_score']['functions'][] = [
                'exp' => [
                    'locations' => [
                        'origin' => [
                            'lat' => $coordinates['lat'],
                            'lon' => $coordinates['lon'],
                        ],
                        'offset' => '1km',
                        'scale'  => '10km',
                    ],
                ],
            ];
        }

        $ESQuery['function_score']['functions'][] = [
            'filter' => [
                [
                    'nested' => [
                        'path'  => 'appointments',
                        'query' =>
                            [
                                'bool' => [
                                    'must' => [
                                        ['exists' => ['field' => 'appointments']],
                                        [
                                            'range' => [
                                                'appointments.start_date' => [
                                                    'gte' => 'now',
                                                ],
                                            ],
                                        ],

                                    ],
                                ],
                            ],
                    ],
                ],
            ],
            'weight' => 5,
            'exp'    => [
                'appointments.start_date' => [
                    'origin' => 'now',
                    'offset' => '1h',
                    'scale'  => '1d',
                ],
            ],
        ];

        $source = Schema::getColumnListing($this->doctor->getTable());
        $ESDoctors = $this->doctor->searchByQuery($ESQuery, null, $source, $limit, null, null);

        $includes = $this->doctor->getRelations();
        $ESDoctors->load($includes);

        $isTelemedicine = (int)$request->get('is_telemedicine', 0);
        $defaultStartDate = (new Carbon())->setTimezone('America/Toronto')->addMinutes(30)->format('Y-m-d H:i:s');
        $startDate = $request->get('start_date', $defaultStartDate);
        $endDate = $request->get('end_date', null);
        $ESDoctors->load([
            'appointments' => function ($query) use ($startDate, $endDate, $isTelemedicine) {
                $query->where('is_telemedicine', $isTelemedicine);
                $query->where('start_date', '>=', $startDate);
                $query->where('status', Appointment::STATUS_AVAILABLE);
                if (!is_null($endDate)) {
                    $query->where('end_date', '<=', $endDate);
                }
                $query->orderBy('start_date', 'ASC');
            },
        ]);

        return fractal()->collection($ESDoctors, new DoctorTransformer())
            ->paginateWith(new ElasticquentPaginatorAdapter($ESDoctors->paginate($perPage)))
            ->parseIncludes($includes);
    }

    public function get(DoctorGetRequest $request, $id)
    {
        $doctor = $this->doctor->find($id);
        if ($doctor === null) {
            return new JsonEmptyItemResponse();
        }

        $includes = $this->getIncludes($request, array_merge($this->doctor->getRelations(), [
            'fees.meta',
        ]));
        $doctor->load($includes->toArray());

        $now = (new Carbon())->setTimezone('America/Toronto');
        $startDate = $request->get('start_date', $now->copy()->addMinutes(30)->format('Y-m-d H:i:s'));
        $status = $request->get('status', null);
        $doctor->load([
            'appointments' => function ($query) use ($startDate, $status, $id) {
                $query->where('start_date', '>=', $startDate)->limit(3);
                if (!is_null($status)) {
                    $query->where('status', $status);
                }
            },
        ]);

        return fractal()->item($doctor, new DoctorTransformer())->parseIncludes($includes->toArray());
    }
}
