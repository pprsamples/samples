<?php namespace App\Http\Requests;

use App\Entities\Auth\Role;
use App\User;
use Illuminate\Support\Facades\Auth;

class AdminRequest extends IncludeRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user->hasRole(Role::ADMIN)) {
            return true;
        }

        if ($user->hasRole([Role::CLINIC_ADMIN, Role::STAFF])) {
            $inputClinicIds = $this->input('clinic_ids', []);
            if ($this->has('clinic_id')) {
                $inputClinicIds = [$this->input('clinic_id')];
            }
            $clinics = $user->clinics->whereIn('id', $inputClinicIds);

            return count($clinics);
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), config('rules.clinic_ids'));
    }
}
