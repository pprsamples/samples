<?php namespace App\Http\Requests;

class AdminAppointmentRequest extends AdminRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), config('rules.clinic_ids'), [
            'type_id' => 'required|integer',
        ]);
    }
}
