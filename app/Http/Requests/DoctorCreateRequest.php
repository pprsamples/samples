<?php namespace App\Http\Requests;

class DoctorCreateRequest extends IncludeRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return array_merge(parent::rules(), [
            'first_name'      => 'required|string',
            'last_name'       => 'required|string',
            'clinic_id'       => 'required|integer|exists:clinics,id',
            'specialty_ids'   => 'sometimes|required|array',
            'specialty_ids.*' => 'integer|min:0|exists:specialties,id',
        ]);
    }
}
