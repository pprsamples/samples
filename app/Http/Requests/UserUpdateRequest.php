<?php namespace App\Http\Requests;

use App\User;
use Illuminate\Support\Facades\Auth;

class UserUpdateRequest extends IncludeRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /** @var User $user */
        $user = Auth::user();

        // @todo verify that gender is values 'F', 'M', "" (don't consider caps)
        return array_merge(parent::rules(), [
            'full_name'  => "sometimes|required|string",
            'first_name' => "sometimes|required|string",
            'last_name'  => "sometimes|required|string",
            'birthdate'  => "sometimes|required|date",
            'email'      => "sometimes|required|email|unique:users,email,{$user->id}",
            'phone'      => 'sometimes|required|' . config('rules.phone'),
        ]);
    }
}
