<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class IncludeRequest extends FormRequest implements IIncludeRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(config('rules.includes'), config('rules.statuses'));
    }

    public function getBaseRules()
    {
        return array_merge(config('rules.includes'), config('rules.statuses'));
    }
}
