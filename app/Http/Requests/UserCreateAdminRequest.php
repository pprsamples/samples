<?php namespace App\Http\Requests;

use App\Entities\Auth\Permission;
use App\Entities\Auth\Role;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserCreateAdminRequest extends UserCreateRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user->hasRole('admin')) {
            return true;
        }

        $roleIds = $this->input('role_ids');
        foreach ($roleIds as $roleId) {
            $role = Role::find($roleId);
            $authorized = $user->hasCreatePermissionForRole($role);
            if (!$authorized) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userCreateRules = parent::rules();

        return array_merge($userCreateRules, [
            'role_ids'     => 'required|array',
            'role_ids.*'   => 'integer|min:0|exists:roles,id',
            'clinic_ids'   => 'sometimes|required|array',
            'clinic_ids.*' => 'integer|min:0|exists:clinics,id',
        ]);
    }
}
