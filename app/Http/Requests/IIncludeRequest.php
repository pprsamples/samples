<?php namespace App\Http\Requests;

interface IIncludeRequest
{

    /**
     * Get the base validation rules that apply to the request.
     *
     * @return array
     */
    public function getBaseRules();


}
