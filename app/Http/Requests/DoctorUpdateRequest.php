<?php namespace App\Http\Requests;

class DoctorUpdateRequest extends AdminRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'      => 'string',
            'last_name'       => 'string',
            'status'          => 'digits:1',
            'email'           => 'email',
            'website'         => 'url',
            'phone'           => 'regex:/[0-9]{10}/',
            'specialty_ids'   => 'array',
            'specialty_ids.*' => 'integer|min:0|exists:specialties,id',
            'procedure_ids'   => 'array',
            'procedure_ids.*' => 'integer|min:0|exists:procedures,id',
        ];
    }
}
