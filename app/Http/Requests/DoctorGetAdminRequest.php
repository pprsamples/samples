<?php namespace App\Http\Requests;

class DoctorGetAdminRequest extends AdminRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(),  [
            'start_date' => 'date',
            'end_date'   => 'date',
        ]);
    }
}
