<?php namespace App\Http\Requests;

class UserUpdateAdminRequest extends AdminRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = $this->route('id');

        return array_merge(parent::rules(), [
            'role_ids'     => 'sometimes|required|array',
            'role_ids.*'   => 'integer|min:0|exists:roles,id',
            'clinic_ids'   => 'sometimes|required|array',
            'clinic_ids.*' => 'integer|min:0|exists:clinics,id',

            'full_name'  => "sometimes|required|string",
            'first_name' => "sometimes|required|string",
            'last_name'  => "sometimes|required|string",
            'username'   => "unique:users,username,{$userId}|" . config('rules.username'),
            'email'      => "sometimes|required|email|unique:users,email,{$userId}",
            'phone'      => 'sometimes|required|' . config('rules.phone'),
            'password'   => config('rules.password'),
        ]);
    }
}
