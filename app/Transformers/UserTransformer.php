<?php namespace App\Transformers;

use App\Entities\Auth\Role;
use Illuminate\Support\Collection;
use League\Fractal;
use App\User;

class UserTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = ['roles', 'specialty_appointments', 'clinics', 'markers'];

    public function transform(User $user)
    {
        return [
            'id'         => (int)$user->id,
            'email'      => $user->email,
            'phone'      => $user->phone,
            'birthdate'  => $user->birthdate ? $user->birthdate->toDateString() : null,
            'activated'  => (int)$user->activated,
            'last_login' => $user->last_login,
            'username'   => $user->username,
            'name'       => $user->name,
            'full_name'  => $user->full_name,
            'first_name' => $user->first_name,
            'last_name'  => $user->last_name,
            'zipcode'    => $user->zipcode,
            'locale'     => $user->locale,
            'skype_id'   => $user->skype_id,
            'gender'     => is_null($user->gender) ? '' : $user->gender,
            'avatar_url' => $user->avatar_url,
        ];
    }

    /**
     * Include Roles
     *
     * @param User $user
     *
     * @return Fractal\Resource\Collection
     */
    public function includeRoles(User $user)
    {
        $permissions = $this->getPermissions($user->roles);
        $roles = $this->collection($user->roles, new RoleTransformer());

        return $roles->setMeta([
            'is_admin'    => (boolean)$this->isRoleAdmin($user->roles),
            'permissions' => $permissions,
        ]);
    }

    /**
     * @param User $user
     *
     * @return Fractal\Resource\Collection
     */
    public function includeMarkers(User $user)
    {
        $markers = $user->markers;

        return $this->collection($markers, new MarkerTransformer());
    }

    /**
     * Include Appointments
     *
     * @param User $user
     *
     * @return Fractal\Resource\Collection
     */
    public function includeSpecialtyAppointments(User $user)
    {
        $appointments = $user->specialty_appointments;

        return $this->collection($appointments, new SpecialtyAppointmentTransformer());
    }

    /**
     * Include Clinics
     *
     * @param User $user
     *
     * @return Fractal\Resource\Collection
     */
    public function includeClinics(User $user)
    {
        $clinics = $user->clinics;

        return $this->collection($clinics, new ClinicTransformer());
    }

    public function isRoleAdmin(Collection $roles)
    {
        return $roles->contains(function (Role $role) {
            return in_array($role->name, [Role::ADMIN, Role::STAFF]);
        });
    }

    /**
     * Returns all permissions for user
     *
     * @param Collection $roles
     *
     * @return Collection
     */
    protected function getPermissions(Collection $roles)
    {
        return $roles->pluck('perms')->flatten()->unique('id')->pluck('name');
    }
}
