<?php namespace App\Transformers;

use App\Entities\Doctor;
use Illuminate\Support\Collection;
use League\Fractal;

class DoctorTransformer extends Fractal\TransformerAbstract
{
    public function getAvailableIncludes()
    {
        return (new Doctor())->getRelations();
    }

    public function transform(Doctor $doctor)
    {
        $status = (int)$doctor->status;
        $specialty = $doctor->getSpecialty();

        return [
            'id'           => (int)$doctor->id,
            'first_name'   => $doctor->first_name,
            'last_name'    => $doctor->last_name,
            'label'        => $doctor->is_doctor ? 'doctor' : 'specialist',
            'status'       => $status,
            'specialty_id' => is_null($specialty) ? null : $specialty->id,
            'is_doctor'    => $doctor->is_doctor,
            'lowest_fee'   => $doctor->lowest_fee,
            'gender'       => is_null($doctor->gender) ? '' : $doctor->gender,
            'registered'   => $status === Doctor::STATUS_REGISTERED,
            'email'        => $doctor->email,
            'name'         => $doctor->name,
            'slug'         => $doctor->slug,
            'avatar_url'   => $doctor->avatar_url,
            'bio_en'       => $doctor->bio_en,
            'bio_fr'       => $doctor->bio_fr,
        ];
    }

    public function includeAppointments(Doctor $doctor)
    {
        $appointments = $this->collection($doctor->appointments, new AppointmentTransformer());

        $locations = $this->getLocationsFromAppointments($doctor->appointments);

        return $appointments->setMeta(['locations' => $locations]);
    }

    public function includeAppointmentMetas(Doctor $doctor)
    {
        $attributes = $doctor->appointment_metas;

        return $this->collection($attributes, new SpecialtyAppointmentMetaTransformer());
    }

    public function includeClinics(Doctor $doctor)
    {
        $clinics = $this->collection($doctor->clinics, new ClinicTransformer());

        $locations = $this->getLocationsFromClinics($clinics);

        return $clinics->setMeta(['locations' => $locations]);
    }

    public function includeFeeMetas(Doctor $doctor)
    {
        $fees = $doctor->fee_metas;

        return $this->collection($fees, new SpecialtyFeeMetaTransformer());
    }

    public function includeFees(Doctor $doctor)
    {
        $fees = $doctor->fees;

        return $this->collection($fees, new SpecialtyFeeTransformer());
    }

    public function includeProcedures(Doctor $doctor)
    {
        $procedures = $doctor->procedures;

        $ids = $procedures->pluck('id');

        return $this->collection($procedures, new ProcedureTransformer())->setMeta(['ids' => $ids]);
    }

    public function includeSpecialties(Doctor $doctor)
    {
        $specialties = $doctor->specialties;

        $ids = $specialties->pluck('id');

        return $this->collection($specialties, new SpecialtyTransformer())->setMeta(['specialty_ids' => $ids]);
    }

    protected function getLocationsFromClinics(Fractal\Resource\Collection $clinics)
    {
        $locations = [];
        /** @var \App\Entities\Clinic $clinic */
        foreach ($clinics->getData() as $clinic) {
            $locations[] = [
                'clinic_id'     => $clinic->id,
                'address_1'     => $clinic->address_1,
                'address_2'     => $clinic->address_2,
                'city'          => $clinic->city,
                'state'         => $clinic->state,
                'zipcode'       => $clinic->zipcode,
                'phone_numbers' => [],
                'lat'           => $clinic->latitude,
                'lon'           => $clinic->longitude,
                'position'      => [
                    'lat' => $clinic->latitude,
                    'lng' => $clinic->longitude,
                ],
            ];
        }

        return $locations;
    }

    protected function getLocationsFromAppointments(Collection $appointments)
    {
        $locations = collect();
        /** @var \App\Entities\Appointment $appointment */
        foreach ($appointments as $appointment) {
            $clinic = $appointment->clinic;
            $location = $locations->first(function ($location) use ($clinic) {
                return $location['clinic_id'] === $clinic->id;
            });

            if ($location === null) {
                $locations->push([
                    'clinic_id'     => $clinic->id,
                    'address_1'     => $clinic->address_1,
                    'address_2'     => $clinic->address_2,
                    'city'          => $clinic->city,
                    'state'         => $clinic->state,
                    'zipcode'       => $clinic->zipcode,
                    'phone_numbers' => [],
                    'lat'           => $clinic->latitude,
                    'lon'           => $clinic->longitude,
                    'position'      => [
                        'lat' => $clinic->latitude,
                        'lng' => $clinic->longitude,
                    ],
                ]);
            }
        }

        return $locations;
    }
}
