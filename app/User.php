<?php namespace App;

use App\Entities\Appointment;
use App\Entities\Auth\Permission;
use App\Entities\Auth\Role;
use App\Entities\Clinic;
use App\Entities\Marker;
use App\Entities\SpecialtyAppointment;
use App\Entities\WalkinAppointment;
use App\Notifications\ResetPasswordNotification;
use Elasticquent\ElasticquentTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Laravel\Cashier\Billable;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\MediaLibrary\Media;
use Zizaco\Entrust\Traits\EntrustUserTrait;

/**
 * Class User
 *
 * @package App
 *
 * @property int            id
 * @property string         email
 * @property string         phone
 * @property string         activation_code
 * @property int            activated
 * @property string         name
 * @property string         first_name
 * @property string         last_name
 * @property string         full_name
 * @property string         username
 * @property string         gender
 * @property string         locale
 * @property string         zipcode
 * @property string         skype_id
 * @property string         avatar_url
 * @property string         password
 * @property \Carbon\Carbon birthdate
 * @property Collection     roles
 * @property Collection     markers
 * @property Collection     clinics
 * @property Collection     specialty_appointments
 * @property Collection     walkin_appointments
 */
class User extends Authenticatable implements HasMedia
{
    use EntrustUserTrait, Billable, Notifiable, HasMediaTrait, ElasticquentTrait;

    const INDEX_NAME = 'users';

    protected $guarded           = [];
    protected $table             = 'users';
    protected $hidden            = ['password', 'remember_token'];
    protected $with              = ['roles.perms'];
    protected $dates             = [
        'birthdate',
    ];
    protected $mappingProperties = [
        'full_name' => [
            'type'      => 'text',
            'analyzer'  => 'standard',
            'fielddata' => true,
        ],
        'email'     => [
            'type'     => 'text',
            'analyzer' => 'standard',
        ],
        'birthdate' => [
            'type'   => 'date',
            "format" => "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis",
        ],

    ];

    public function clinics()
    {
        return $this->belongsToMany(Clinic::class, 'clinic_user', 'user_id', 'clinic_id');
    }

    public function markers()
    {
        return $this->belongsToMany(Marker::class, 'marker_user', 'user_id', 'marker_id');
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class, 'user_id', 'id');
    }

    public function specialty_appointments()
    {
        return $this->hasMany(SpecialtyAppointment::class, 'user_id', 'id');
    }

    public function walkin_appointments()
    {
        return $this->hasMany(WalkinAppointment::class, 'user_id', 'id');
    }

    /**
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    /**
     * @return mixed
     */
    public function getAuthPassword()
    {
        return $this->attributes['password'];
    }

    /**
     * Get user full name.
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->full_name;
    }

    /**
     * Get the user's first name.
     *
     * @param  string $value
     *
     * @return string
     */
    public function getFirstNameAttribute($value)
    {
        return isset($value) ? ucwords($value, ' \-') : $value;
    }

    /**
     * Set the user's first name.
     *
     * @param  string $value
     *
     * @return void
     */
    public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = strtolower($value);
    }

    /**
     * Get the user's last name.
     *
     * @param  string $value
     *
     * @return string
     */
    public function getLastNameAttribute($value)
    {
        return isset($value) ? ucwords($value, ' \-') : $value;
    }

    /**
     * Set the user's last name.
     *
     * @param  string $value
     *
     * @return void
     */
    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = strtolower($value);
    }

    /**
     * Set the user's full name.
     *
     * @param  string $value
     *
     * @return void
     */
    public function setFullNameAttribute($value)
    {
        $this->attributes['full_name'] = strtolower($value);
    }

    /**
     * Return the media url for the user avatar
     *
     * @return string
     */
    public function getAvatarUrlAttribute()
    {
        /** @var Media $avatar */
        $avatar = $this->getMedia('users.avatar')->first();
        $avatarUrl = '/images/default_avatar.png';
        if (!is_null($avatar) && file_exists(public_path() . $avatar->getUrl())) {
            $avatarUrl = $avatar->getUrl();
        }

        return $avatarUrl;
    }

    /**
     * Set the user's email.
     *
     * @param  string $value
     *
     * @return void
     */
    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }

    public function routeNotificationForSlack()
    {
        return config('slack.endpoint');
    }

    /**
     * @param array $array
     *
     * @return bool
     */
    protected function isAdmin($array = [])
    {
        if (array_key_exists('roles', $array)) {
            $collection = collect($array['roles']);

            return $collection->contains(function ($value) {
                return in_array($value['name'], ['root', 'admin', 'staff']);
            });
        }

        return false;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = parent::toArray();
        $array['is_admin'] = $this->isAdmin($array);

        return $array;
    }

    public function hasCreatePermissionForRole(Role $role)
    {
        if ($role->name === Role::CLINIC_ADMIN && $this->can(Permission::CREATE_CLINIC_ADMIN)) {
            return true;
        } elseif ($role->name === Role::STAFF && $this->can(Permission::CREATE_STAFF)) {
            return true;
        } elseif ($role->name === Role::PATIENT && $this->can(Permission::CREATE_PATIENT)) {
            return true;
        }

        return false;
    }

    /**
     * Send the password reset notification.
     *
     * @param string $token
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public static function createIndexWithCustomAnalyzer($shards = null, $replicas = null)
    {
        $instance = new static;

        $client = $instance->getElasticSearchClient();

        $indexName = config('elasticquent.index.users');

        $params = [
            'index' => $indexName,
            'body'  => [
                'settings' => [
                    'number_of_shards'   => $shards,
                    'number_of_replicas' => $replicas,
                    'analysis'           => [
                        'analyzer' => $instance->customAnalyzer,
                    ],
                ],
                'mappings' => [
                    $indexName => [
                        'properties' => $instance->mappingProperties,
                    ],
                ],
            ],
        ];

        return $client->indices()->create($params);
    }

    /**
     * Delete Elasticsearch index
     *
     * @return array
     */
    public static function deleteIndex()
    {
        $instance = new static;

        /** @var \Elasticsearch\Client $client */
        $client = $instance->getElasticSearchClient();
        $deleteParams = [
            'index' => config('elasticquent.index.users'),
        ];
        $response = $client->indices()->delete($deleteParams);

        return $response;
    }

    /**
     * Return Elasticsearch index_name
     *
     * @return string
     */
    public function getIndexName()
    {
        return config('elasticquent.index.users');
    }

    /**
     * Return Elasticsearch type_name
     *
     * @return mixed
     */
    public function getTypeName()
    {
        return config('elasticquent.index.users');
    }

    /**
     * Update Elasticsearch document
     *
     * @return array
     */
    public function updateDocument()
    {
        $data = $this->getIndexDocumentData();
        $params = [
            'index' => config('elasticquent.index.users'),
            'type'  => config('elasticquent.index.users'),
            'id'    => $this->id,
            'body'  => [
                'doc' =>
                    $data,

            ],
        ];

        return $this->getElasticSearchClient()->update($params);
    }
}
