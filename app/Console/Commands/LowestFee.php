<?php namespace App\Console\Commands;

use App\Entities\Clinic;
use App\Entities\Doctor;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class LowestFee extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:lowest-fee {--chunk=100}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets doctor flag on doctor table based on specialties';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Starting modifications');
        $doctorCount = Doctor::count();
        $this->info("Doctors count: {$doctorCount}");
        $chunkSize = $this->option('chunk');
        $this->info("Chunk size: {$chunkSize}");
        $this->info("Chunks: " . ceil($doctorCount / $chunkSize));

        $count = 1;
        Doctor::with(['fees'])->chunk($chunkSize, function (Collection $specialists) use (&$count) {
            $this->info("processing chunk {$count}");
            $specialists->each(function (Doctor $specialist) {
                // Exclude Rolla Hraibeh for now
                if ($specialist->id !== 279) {
                    $lowestFee = $specialist->fees->min('amount');
                    $specialist->lowest_fee = $lowestFee;
                    $specialist->save();
                }
            });
            $count++;
        });
        $this->info('Done Doctors');

        $this->info('--');
        $clinicCount = Clinic::count();
        $this->info("Clinics count: {$clinicCount}");
        $chunkSize = $this->option('chunk');
        $this->info("Chunk size: {$chunkSize}");
        $this->info("Chunks: " . ceil($clinicCount / $chunkSize));

        $count = 1;
        Clinic::with(['fees'])->chunk($chunkSize, function (Collection $clinics) use (&$count) {
            $this->info("processing chunk {$count}");
            $clinics->each(function (Clinic $clinic) {
                $lowestFee = $clinic->fees->min('amount');
                $clinic->lowest_fee = $lowestFee;
                $clinic->save();
            });
            $count++;
        });
        $this->info('Done Clinics');
    }
}
