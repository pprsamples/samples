<?php namespace App\Mail;

use Illuminate\Mail\Mailable;

abstract class LocalizedMailable extends Mailable
{
    protected $notifiableLocale = 'en';

    public function __construct($locale = 'en')
    {
        $this->notifiableLocale = $locale;
    }

    /**
     * @param string $viewName
     * @param string $locale
     *
     * @return string
     */
    public function getLocalizedView($viewName, string $locale = null)
    {
        $locales = config('app.locales', []);
        if (is_null($locale)) {
            $locale = $this->notifiableLocale;
        }

        if (!in_array($locale, $locales)) {
            $locale = config('app.locale', 'default');
        }

        $localedViewName = $viewName . '_' . $locale;
        if (view()->exists($localedViewName)) {
            return $localedViewName;
        }

        return $viewName;
    }
}