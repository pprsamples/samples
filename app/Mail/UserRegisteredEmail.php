<?php namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class UserRegisteredEmail extends LocalizedMailable
{
    use Queueable, SerializesModels;

    /** @var User */
    private $user;

    /**
     * Create a new message instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $viewName = $this->getLocalizedView('emails.patient.registered', $this->user->locale);

        return $this->subject(__('Thanks for registering'))
            ->markdown($viewName)->with([
                'title'   => __('Thank you for signing up to docallo') . '!',
                'content' => [
                    'name' => $this->user->name,
                ],
                'url'     => route('home'),
            ]);
    }
}
