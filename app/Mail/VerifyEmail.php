<?php namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class VerifyEmail extends LocalizedMailable
{
    use Queueable, SerializesModels;

    /**
     * The user instance.
     *
     * @var User
     */
    private $user;

    /**
     * Creates a new message instance.
     *
     * @param User $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Builds the message.
     *
     * @return $this
     * @throws \Exception
     */
    public function build()
    {
        $viewName = $this->getLocalizedView('emails.verify', $this->user->locale);

        if (is_null($this->user->activation_code)) {
            throw new \Exception('User does not have an activation code');
        }

        return $this->markdown($viewName)
            ->subject(__('Thank you for signing up with docallo') . '!')
            ->with([
                'name'  => $this->user->name,
                'email' => $this->user->email,
                'url'   => route('users.activate.' . $this->user->locale, ['code' => $this->user->activation_code]),
            ])
            ->from(config('constants.DO_NOT_REPLY_MAIL_ADDRESS'), __('docallo'));
    }
}