<?php namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\SlackAttachment;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\App;

class AdminAlertNotification extends Notification
{
    use Queueable;

    /** @var string */
    private $message;

    /** @var array */
    private $attachmentFields;

    /** @var string */
    private $channel;

    /**
     * Create a new notification instance.
     *
     * @param string $message
     * @param string $channel
     * @param array  $attachmentFields
     */
    public function __construct($message, $channel = '#admin_alerts', array $attachmentFields = [])
    {
        $this->message = $message;
        $this->attachmentFields = $attachmentFields;
        $this->channel = $channel;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return App::environment('local') ? ['mail'] : ['slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->line($this->message)
            ->with($this->attachmentFields);
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return SlackMessage
     */
    public function toSlack($notifiable)
    {
        $slackMessage = (new SlackMessage)
            ->to($this->channel)
            ->content($this->message);

        if (!empty($this->attachmentFields)) {
            $slackMessage->attachment(function (SlackAttachment $attachment) {
                $attachment->fields($this->attachmentFields);
            });
        }

        return $slackMessage;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
