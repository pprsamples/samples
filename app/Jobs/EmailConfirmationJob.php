<?php namespace App\Jobs;

use App\Jobs\AbstractNotificationJob;
use App\Mail\VerifyEmail;
use App\User;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\App;
use Psr\Log\LoggerInterface;

class EmailConfirmationJob extends AbstractNotificationJob
{
    /**
     * @var \App\User
     */
    protected $user;

    /**
     * EmailRegistration constructor.
     *
     * @param \App\User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function handle(LoggerInterface $logger)
    {
        App::setLocale($this->user->locale);

        $this->getMailer()->to($this->user->email, $this->user->name)
            ->send(new VerifyEmail($this->user));

        if ($failed = $this->getMailer()->failures()) {
            foreach ($failed as $recipient) {
                $logger->error('Failed to send message [emails.verify] to ' . $recipient);
            }
        }

        $this->delete();
    }
}
