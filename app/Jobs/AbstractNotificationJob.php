<?php

namespace App\Jobs;

use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

abstract class AbstractNotificationJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Get mailer instance.
     *
     * @return \Illuminate\Contracts\Mail\Mailer
     */
    protected function getMailer()
    {
        return app(Mailer::class);
    }
}
