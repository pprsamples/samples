<?php namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Elasticquent\ElasticquentTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\MediaLibrary\Media;

/**
 * Class Doctor
 *
 * @package App\Entities
 *
 * @property int             id
 * @property string          first_name
 * @property string          last_name
 * @property int             status
 * @property int             is_doctor
 * @property int             lowest_fee
 * @property string          gender
 * @property string          email
 * @property string          name
 * @property string          slug
 * @property string          avatar_url
 * @property string          bio_en
 * @property string          bio_fr
 * @property-read Collection specialties
 * @property-read Collection procedures
 * @property-read Collection clinics
 * @property-read Collection appointments
 * @property-read Collection fees
 * @property-read Collection appointment_metas
 * @property-read Collection fee_metas
 */
class Doctor extends Model implements HasMedia
{
    use ElasticquentTrait, SoftDeletes, HasMediaTrait;

    const INDEX_NAME = 'doctors';

    const STATUS_NON_REGISTERED   = 0;
    const STATUS_DISABLED         = 1;
    const STATUS_REGISTERED       = 2;
    const STATUS_WALK_IN          = 3;
    const STATUS_PENDING_APPROVAL = 4;

    protected $table                  = 'doctors';
    protected $primaryKey             = 'id';
    protected $guarded                = [];
    protected $hidden                 = ['pivot'];
    protected $relations              = [
        'specialties',
        'clinics',
        'procedures',
        'fee_metas',
        'fees',
        'appointments',
    ];
    protected $elasticSearchRelations = ['specialties', 'clinics', 'appointments', 'procedures'];

    protected $casts             = [
        'status'     => 'integer',
        'is_doctor'  => 'integer',
        'lowest_fee' => 'integer',
    ];
    protected $mappingProperties = [
        'name'         => [
            'type'     => 'text',
            'analyzer' => 'standard',
        ],
        'specialty'    => [
            'type'     => 'text',
            'analyzer' => 'standard',
        ],
        'locations'    => [
            'type' => 'geo_point',
        ],
        'appointments' => [
            'type'       => 'nested',
            'properties' => [
                'location'   => [
                    'type' => 'geo_point',
                ],
                'start_date' => [
                    'type'   => 'date',
                    "format" => "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis",
                ],
                'end_date'   => [
                    'type'   => 'date',
                    "format" => "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis",
                ],
            ],
        ],
    ];

    public static function createIndexWithCustomAnalyzer($shards = null, $replicas = null)
    {
        $instance = new static;

        $client = $instance->getElasticSearchClient();

        $indexName = config('elasticquent.index.doctors');

        $params = [
            'index' => $indexName,
            'body'  => [
                'settings' => [
                    'number_of_shards'   => $shards,
                    'number_of_replicas' => $replicas,
                    'analysis'           => [
                        'analyzer' => $instance->customAnalyzer,
                    ],
                ],
                'mappings' => [
                    $indexName => [
                        'properties' => $instance->mappingProperties,
                    ],
                ],
            ],
        ];

        return $client->indices()->create($params);
    }

    /**
     * Delete Elasticsearch index
     *
     * @return array
     */
    public static function deleteIndex()
    {
        $instance = new static;

        /** @var \Elasticsearch\Client $client */
        $client = $instance->getElasticSearchClient();
        $deleteParams = [
            'index' => config('elasticquent.index.doctors'),
        ];
        $response = $client->indices()->delete($deleteParams);

        return $response;
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class, 'doctor_id', 'id');
    }


    public function clinics()
    {
        return $this->belongsToMany(Clinic::class, 'clinic_doctor', 'doctor_id', 'clinic_id');
    }

    public function fee_metas()
    {
        return $this->hasMany(SpecialtyFeeMeta::class, 'doctor_id', 'id');
    }

    public function fees()
    {
        return $this->belongsToMany(SpecialtyFee::class, 'specialty_fees', 'doctor_id', 'fee_id');
    }

    public function procedures()
    {
        return $this->belongsToMany(Procedure::class, 'doctor_procedure', 'doctor_id', 'procedure_id');
    }

    public function specialties()
    {
        return $this->belongsToMany(Specialty::class, 'doctor_specialty', 'doctor_id', 'specialty_id');
    }

    /**
     * Get specialist's specialty
     * Temp while specialist have only one specialty
     *
     * @return Specialty
     */
    public function getSpecialty()
    {
        return $this->specialties()->first();
    }

    /**
     * Return doctor full name.
     *
     * @return string
     */
    public function getNameAttribute()
    {
        $name = "{$this->first_name} {$this->last_name}";
        if ($this->is_doctor) {
            $name = 'Dr ' . $this->last_name . ', ' . $this->first_name;
        }

        return $name;
    }

    /**
     * Return doctor slug.
     *
     * @return string
     */
    public function getSlugAttribute()
    {
        return str_slug($this->name) . '-' . $this->id;
    }

    /**
     * Return the media url for the doctor avatar
     *
     * @return string
     */
    public function getAvatarUrlAttribute()
    {
        /** @var Media $avatar */
        $avatar = $this->getMedia('doctors.avatar')->first();
        $avatarUrl = $this->gender === 'F' ? 'img/avatar_female.svg' : 'img/avatar_male.svg';
        if (!is_null($avatar) && file_exists(public_path() . $avatar->getUrl())) {
            $avatarUrl = $avatar->getUrl();
        }
        // remove leading slash from the returned URL
        $avatarUrl = ltrim($avatarUrl, '/');

        return $avatarUrl;
    }

    /**
     * Return doctor first_name
     *
     * @param string $value
     *
     * @return string
     */
    public function getFirstNameAttribute($value)
    {
        return ucfirst($value);
    }

    /**
     * Return doctor last_name
     *
     * @param string $value
     *
     * @return string
     */
    public function getLastNameAttribute($value)
    {
        return ucfirst($value);
    }

    /**
     * Return Elasticsearch index_name
     *
     * @return string
     */
    public function getIndexName()
    {
        return config('elasticquent.index.doctors');
    }

    /**
     * Return Elasticsearch type_name
     *
     * @return mixed
     */
    public function getTypeName()
    {
        return config('elasticquent.index.doctors');
    }

    public function getElasticSearchRelations()
    {
        return $this->elasticSearchRelations;
    }

    /**
     * Update Elasticsearch document
     *
     * @return array
     */
    public function updateDocument()
    {
        $data = $this->getIndexDocumentData();
        $params = [
            'index' => config('elasticquent.index.doctors'),
            'type'  => config('elasticquent.index.doctors'),
            'id'    => $this->id,
            'body'  => [
                'doc' =>
                    $data,

            ],
        ];

        return $this->getElasticSearchClient()->update($params);
    }

    /**
     * Override Elasticquent getIndexDocumentData function
     *
     * @return array
     */
    public function getIndexDocumentData()
    {
        $data = $this->toArray();
        $data['locations'] = $this->clinics->map(function (Clinic $clinic) {
            return $clinic->getLocationAttribute();
        });
        $data['appointments'] = $this->appointments()
            ->where('status', Appointment::STATUS_AVAILABLE)
            ->whereDate('start_date', '>=', (new Carbon())->toDateString())
            ->limit(3)
            ->get();

        return $data;
    }

    /**
     * Determines if Specialist is a doctor based on his specialties
     *
     * @return bool
     */
    public function isDoctor()
    {
        return $this->specialties->contains(function (Specialty $specialty) {
            return $specialty->is_doctor;
        });
    }
}
