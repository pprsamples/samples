<?php namespace App\Recipients;

class AdminRecipient extends Recipient
{
    public function __construct()
    {
        $this->email = config('constants.ADMIN_EMAIL', 'default@email.com');
    }

    public function routeNotificationForSlack()
    {
        return config('constants.FAILED_JOB_SLACK_WEBHOOK_URL');
    }

    public function getKey()
    {
        return $this->email;
    }

}