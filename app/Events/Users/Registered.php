<?php

namespace App\Events\Users;

use App\User;
use Illuminate\Queue\SerializesModels;

class Registered
{
    use SerializesModels;

    /**
     * Registered user instance.
     *
     * @var \App\User
     */
    public $user;

    /**
     * Registered constructor.
     *
     * @param \App\User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
